package com.pierreduchemin.punchlinebingo.models;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * LocalizedPunchlines local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LocalizedPunchlinesTest {

    private static final int NB_LOCALE = 2;

    @Test
    public void checkLocaleNumber() throws Exception {
        int nbLocale = LocalizedPunchlines.values().length;

        // All locales are tested
        assertEquals(NB_LOCALE, nbLocale);
    }

    @Test
    public void getLocale() throws Exception {
        Locale localeFr = LocalizedPunchlines.FR.getLocale();
        Locale localeEn = LocalizedPunchlines.EN.getLocale();

        // No misconception between locales
        assertEquals(Locale.FRENCH, localeFr);
        assertNotEquals(Locale.FRANCE, localeFr);
        assertNotEquals(Locale.CANADA_FRENCH, localeFr);

        assertEquals(Locale.ENGLISH, localeEn);
        assertNotEquals(Locale.US, localeEn);
        assertNotEquals(Locale.UK, localeEn);
    }

    @Test
    public void getPunchlineFile() throws Exception {
        int localeFileFr = LocalizedPunchlines.FR.getPunchlineFile();
        int localeFileEn = LocalizedPunchlines.EN.getPunchlineFile();

        assertTrue(localeFileFr > 0);
        assertTrue(localeFileEn > 0);
    }

    @Test
    public void getFileFromLanguage() throws Exception {
        int fileFromLanguageEn = LocalizedPunchlines.getFileFromLanguage(Locale.ENGLISH.getLanguage());
        int fileFromLanguageUk = LocalizedPunchlines.getFileFromLanguage(Locale.UK.getLanguage());
        int fileFromLanguageUs = LocalizedPunchlines.getFileFromLanguage(Locale.US.getLanguage());

        int fileFromLanguageFr = LocalizedPunchlines.getFileFromLanguage(Locale.FRENCH.getLanguage());
        int fileFromLanguageFrCa = LocalizedPunchlines.getFileFromLanguage(Locale.CANADA_FRENCH.getLanguage());

        // At the moment there is no punchlines available in Korean
        int fileFromLanguageKo = LocalizedPunchlines.getFileFromLanguage(Locale.KOREAN.getLanguage());

        assertTrue(fileFromLanguageEn > 0);
        assertTrue(fileFromLanguageFr > 0);

        assertEquals(fileFromLanguageEn, fileFromLanguageUk);
        assertEquals(fileFromLanguageEn, fileFromLanguageUs);
        assertEquals(fileFromLanguageEn, fileFromLanguageKo);

        assertEquals(fileFromLanguageFr, fileFromLanguageFrCa);
    }

}