/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.pierreduchemin.punchlinebingo.PunchlineBingo;
import com.pierreduchemin.punchlinebingo.R;
import com.pierreduchemin.punchlinebingo.adapters.PunchlinesSetAdapter;
import com.pierreduchemin.punchlinebingo.models.PunchlinesSet;
import com.pierreduchemin.punchlinebingo.utils.FileSystemUtils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PunchlinesSetSelectorActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_FILE_PICKER = 1000;

    @BindView(R.id.rvPunchlinesSetSelector)
    RecyclerView rvPunchlinesSetSelector;
    @BindView(R.id.btnLoadFile)
    Button btnLoadFile;

    private PunchlinesSetAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punchlines_set_selector);
        ButterKnife.bind(this);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        rvPunchlinesSetSelector.setHasFixedSize(true);
        rvPunchlinesSetSelector.addItemDecoration(new DividerItemDecoration(rvPunchlinesSetSelector.getContext(), DividerItemDecoration.VERTICAL));
        rvPunchlinesSetSelector.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PunchlinesSetAdapter(PunchlineBingo.getPunchlinesSets());
        rvPunchlinesSetSelector.setAdapter(adapter);
    }

    @OnClick(R.id.btnLoadFile)
    public void onClickLoadFile(View view) {
        new MaterialFilePicker()
                .withCloseMenu(true)
                .withActivity(this)
                .withRootPath("/")
                .withRequestCode(REQUEST_CODE_FILE_PICKER)
                .withFilter(Pattern.compile(".*\\.plb$"))
                .withFilter(Pattern.compile(".*\\.json$"))
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FILE_PICKER && resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
            try {
                InputStream is = new BufferedInputStream(new FileInputStream(filePath));
                List<PunchlinesSet> punchlinesFromJson = FileSystemUtils.getPunchlinesFromJson(is);
                PunchlineBingo.getPunchlinesSets().addAll(punchlinesFromJson);
                adapter.notifyDataSetChanged();
            } catch (IOException e) {
                new AlertDialog.Builder(PunchlinesSetSelectorActivity.this)
                        .setTitle(R.string.error_title_error)
                        .setMessage(R.string.error_message_not_able_to_load_punchlines)
                        .setNegativeButton(android.R.string.ok, null)
                        .setIcon(R.drawable.vr_alert)
                        .show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
