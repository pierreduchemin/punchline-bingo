/*
 * Copyright © 2017.
 * This file is part of Punchline Bingo.
 *
 * Punchline Bingo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Punchline Bingo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Punchline Bingo.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.pierreduchemin.punchlinebingo.models;

import android.support.annotation.NonNull;
import android.support.annotation.RawRes;

import com.pierreduchemin.punchlinebingo.R;

import java.util.Locale;

public enum LocalizedPunchlines {

    EN(Locale.ENGLISH, R.raw.punchlines_en),
    FR(Locale.FRENCH, R.raw.punchlines_fr);

    private final Locale locale;
    @RawRes
    private final int punchlineFile;

    LocalizedPunchlines(Locale locale, int punchlineFile) {
        this.locale = locale;
        this.punchlineFile = punchlineFile;
    }

    public Locale getLocale() {
        return locale;
    }

    @RawRes
    public int getPunchlineFile() {
        return punchlineFile;
    }

    @RawRes
    public static int getFileFromLanguage(@NonNull String language) {
        for (LocalizedPunchlines localizedPunchlines : values()) {
            if (localizedPunchlines.getLocale().getLanguage().equals(language)) {
                return localizedPunchlines.getPunchlineFile();
            }
        }
        return EN.getPunchlineFile();
    }
}
